/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_3;

/**
 *
 * @author lucas
 */
public class ContaBancaria {
    private String Numero;
    private String Agencia;
    private String DataDeAbertura;
    private Cliente DadosPessoais;
    private double Saldo;
    

    public ContaBancaria(String Numero, Cliente DadosPessoais) {
        this.Numero = Numero;
        this.DadosPessoais = DadosPessoais;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String Numero) {
        this.Numero = Numero;
    }

    public String getAgencia() {
        return Agencia;
    }

    public void setAgencia(String Agencia) {
        this.Agencia = Agencia;
    }

    public String getDataDeAbertura() {
        return DataDeAbertura;
    }

    public void setDataDeAbertura(String DataDeAbertura) {
        this.DataDeAbertura = DataDeAbertura;
    }

    public Cliente getDadosPessoais() {
        return DadosPessoais;
    }

    public void setDadosPessoais(Cliente DadosPessoais) {
        this.DadosPessoais = DadosPessoais;
    }
    public double getSaldo() {
        return Saldo;
    }

    public void setSaldo(double Saldo) {
        this.Saldo = Saldo;
    }
}
